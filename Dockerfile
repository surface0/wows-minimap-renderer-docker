FROM python:3.10

RUN pip install git+https://github.com/WoWs-Builder-Team/minimap_renderer.git

COPY entrypoint /bin/entrypoint
RUN chmod a+x /bin/entrypoint

ENTRYPOINT ["/bin/entrypoint"]
