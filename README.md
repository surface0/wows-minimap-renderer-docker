# World of Warships Minimap Renderer for Docker

WoWs用のリプレイマップ描画ツールをDockerで使用するものです。

[GitHub - WoWs-Builder-Team/minimap_renderer: Generates a video of your minimap.](https://github.com/WoWs-Builder-Team/minimap_renderer)

Dockerイメージ化することでPythonやらのインストールを不要にしました。  
ファイル名での入力しかなかったのがやや面倒だったので、標準入力でリプレイファイルを入力して、標準出力で受け取れるようにしました。

## ビルド方法

```shell
$ docker build -t wows-minimap-renderer --no-cache https://gitlab.com/surface0/wows-minimap-renderer-docker.git#main
```

## 使い方

リプレイファイルを標準入力から流し込みます。

```shell
$ docker run --rm -i wows-minimap-renderer < {リプレイファイル名}.wowsreplay > {出力動画ファイル名}.mp4
```

## 注意

WoWs本体のバージョンが更新された場合は、リプレイファイルの仕様が変更されている可能性があります。  
動画変換がうまくいかない場合は、ソースコードのリポジトリに更新が無いか確認して、必要に応じてイメージの再ビルドをしてください。
